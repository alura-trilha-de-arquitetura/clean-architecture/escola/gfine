package br.com.lucaslagasse.gfine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GfineApplication {

	public static void main(String[] args) {
		SpringApplication.run(GfineApplication.class, args);
	}

}
