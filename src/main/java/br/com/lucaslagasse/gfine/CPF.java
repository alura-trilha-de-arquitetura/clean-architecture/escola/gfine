package src.main.java.br.com.lucaslagasse.gfine;

public class CPF {
    
    private String cpf;

    public CPF (String cpf){
        if(cpf == null || 
            !cpf.matches("/(^\d{3}\.\d{3}\.\d{3}\-\d{2}$)|(^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$)/")){
                throw new IllegalArgumentException("CPF inválido!");
        }
        this.cpf = cpf;
    }

}
